local character = require("src/character")
local background = require("src/background")
local islands = require("src/islands")
local coins = require("src/coins")
local hearts = require("src/hearts")
local util = require("src/util")

local maskCanvas

local deathFont
local smallFont
local normalFont
local scoreFont
local heart

local birds
local deathTimer = 0

local function reset()
  deathTimer = 0
  character.reset()
  coins.reset()
  hearts.reset()
  islands.reset()
end

function love.load()
  love.graphics.setDefaultFilter("nearest", "nearest")

  deathFont = love.graphics.newFont("assets/PressStart2P-Regular.ttf", 64)
  smallFont = love.graphics.newFont("assets/PressStart2P-Regular.ttf", 8)
  normalFont = love.graphics.newFont("assets/PressStart2P-Regular.ttf", 16)
  scoreFont = love.graphics.newFont("assets/PressStart2P-Regular.ttf", 20)
  heart = love.graphics.newImage("assets/heart.png")

  birds = love.audio.newSource("assets/birds.mp3", "stream")
  birds:setVolume(0.6)
  birds:setLooping(true)
  birds:play()

  background.load()
  character.load()
  islands.load()
  coins.load()
  hearts.load()

  maskCanvas = love.graphics.newCanvas()
end

function love.update(dt)
  character.update(dt)
  background.update(dt)
  coins.update(dt)
  hearts.update(dt)

  local w, h = love.graphics.getDimensions()
  islands.update(dt, w / 2, h / 2, character.x, character.y)

  if character.fell then
    deathTimer = deathTimer + dt
  end
end

function love.keyreleased(key)
  if key == "r" and character.fell and deathTimer > 0.7 then
    if character.score > character.highScore then
      character.highScore = character.score
      love.filesystem.write("highscore", tostring(character.highScore))
    end

    reset()
  end
end

local function drawScore(text, x, y)
  love.graphics.setFont(scoreFont)
  local th = math.floor(scoreFont:getHeight())
  love.graphics.setColor(0, 0, 0)
  local dirs = {{-2, -2}, {-2, 0}, {-2, 2}, {0, -2}, {0, 0}, {0, 2}, {2, -2}, {2, 0}, {2, 2}}
  for _, dir in ipairs(dirs) do
    love.graphics.print(text, x + dir[1], y + th + dir[2])
  end
  love.graphics.setColor(1, 1, 1)
  love.graphics.print(text, x, y + th)
end

function love.draw()
  local w, h = love.graphics.getDimensions()
  if w ~= maskCanvas:getWidth() or h ~= maskCanvas:getHeight() then
    maskCanvas = love.graphics.newCanvas()
  end

  love.graphics.push()

  love.graphics.scale(2)
  background.draw(character.x, character.y, w / 2, h / 2)
  love.graphics.translate(w / 4 - character.x, h / 4 - character.y)
  
  love.graphics.setCanvas(maskCanvas)
  love.graphics.clear(0, 0, 0)
  islands.drawMasks()
  love.graphics.setCanvas()

  love.graphics.setFont(smallFont)
  islands.draw()
  coins.draw()
  hearts.draw()
  character.draw(maskCanvas)

  love.graphics.pop()

  love.graphics.push()
  love.graphics.scale(2)
  for i = 1, character.lives do
    love.graphics.draw(heart, (i - 1) * 30, h/2 - 40)
  end
  love.graphics.pop()

  if deathTimer > 0.7 then
    local time = deathTimer - 0.7
    local alpha = util.ease(math.min(time / 0.5, 1))

    love.graphics.setColor(0.0, 0.0, 0.0, alpha * 0.9)
    love.graphics.rectangle("fill", 0, 0, w, h)

    love.graphics.setColor(0.8, 0.2, 0.1, alpha)

    love.graphics.setFont(deathFont)
    local text = "YOU DIED"
    local tw = deathFont:getWidth(text)
    local th = deathFont:getHeight()
    love.graphics.print(text, math.floor((w - tw) / 2), math.floor((h - th) / 2))
    love.graphics.setColor(1, 1, 1)

    love.graphics.setFont(normalFont)
    local text = "Press R to respawn"
    local tw = normalFont:getWidth(text)
    local th = normalFont:getHeight()
    love.graphics.print(text, math.floor((w - tw) / 2), 80 + math.floor((h - th) / 2))
    love.graphics.setColor(1, 1, 1)
  end

  love.graphics.setFont(scoreFont)
  local text = "Score: " .. tostring(character.score)
  local th = math.floor(scoreFont:getHeight())
  love.graphics.setColor(0, 0, 0)
  local dirs = {{-2, -2}, {-2, 0}, {-2, 2}, {0, -2}, {0, 0}, {0, 2}, {2, -2}, {2, 0}, {2, 2}}
  for _, dir in ipairs(dirs) do
    love.graphics.print(text, 20 + dir[1], 20 + th + dir[2])
  end
  love.graphics.setColor(1, 1, 1)
  love.graphics.print(text, 20, 20 + th)

  drawScore("Score: " .. tostring(character.score), 20, 20)
  drawScore("Best:  " .. tostring(character.highScore), 20, 60)
end
