function love.conf(t)
  t.identity = "gaben-hl3"
  t.window.resizable = true
  t.window.title = "SKOK"
  t.window.icon = "assets/icon.png"
end
