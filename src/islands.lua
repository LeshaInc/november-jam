local coins = require("src/coins")
local hearts = require("src/hearts")
local util = require("src/util")

local islands = {
  list = {},
}

local physicsScaleX = 86 / 2
local physicsScaleY = 64 / 2
local ccx, ccy = 0, 0 -- yeah i know

local sizes = {
  spawn = 2.32,
  small = 1,
  bolt = 1.45,
  ufo = 0.95,
  medium = 1.36,
  mushroom = 1,
  mushroom2 = 1,
  big = 2,
  god = 1.6,
}

function islands.load()
  islands.assets = util.loadRecursive("assets/islands")
  islands.circleShapes = {}
  for name, size in pairs(sizes) do
    islands.circleShapes[name] = love.physics.newCircleShape(size)
  end
  islands.reset()
end

function islands.reset()
  islands.list = {}
  islands.world = love.physics.newWorld(0, 0, true)

  -- islands.world:setCallbacks(function(a, b, contact)
  --   local x, y = contact:getPositions()
  --   x = x * physicsScaleX
  --   y = y * physicsScaleY

  --   local dist = math.sqrt((x - ccx)^2 + (y - ccy)^2)
  --   -- print(50 / dist)

  --   local sound = islands.assets.kick
  --   if sound:isPlaying() then
  --     sound = sound:clone()
  --   end

  --   local volume = math.min(1, math.max(0, 30 / dist))
  --   if volume > 0.2 then
  --     sound:setVolume(volume * 0.3)
  --     sound:play()
  --   end
  -- end)

  islands.spawn(0, 0, "spawn")
end

function islands.spawn(x, y, type)
  if not type then
    local rand = math.random(1, 111)
    if rand > 109 then
      type = "god"
    elseif rand > 105 then
      type = "bolt"
    elseif rand > 100 then
      type = "ufo"
    elseif rand > 90 then
      type = "mushroom"
    elseif rand > 80 then
      type = "mushroom2"
    elseif rand > 60 then
      type = "medium"
    elseif rand > 30 then
      type = "small"
    else
      type = "big"
    end
  end

  local vel = math.random(1.0, 5.0)
  if type == "spawn" then
    vel = 0
  end
  local angle = math.random(0.0, 2.0 * math.pi)

  local island = {x = x, y = y, vx = 0, vy = 0, type = type}

  island.body = love.physics.newBody(islands.world, x / physicsScaleX, y / physicsScaleY, "dynamic")
  island.body:setLinearVelocity(vel * math.cos(angle), vel * math.sin(angle))
  island.fixture = love.physics.newFixture(island.body, islands.circleShapes[type], 5)
  island.fixture:setRestitution(0.9)
  island.fixture:setFriction(0.0)

  if type ~= "spawn" and type ~= "god" and math.random(1, 10) > 6 then
    island.coin = coins.spawn(x, y, island)
  end

  if type == "god" and math.random(1, 10) > 3 then
    island.heart = hearts.spawn(x, y, island)
  end

  table.insert(islands.list, island)
end

function islands.update(dt, w, h, cx, cy)
  ccx = cx
  ccy = cy
  islands.world:update(dt)

  local remove = {}
  for idx, island in ipairs(islands.list) do
    island.x = island.body:getX() * physicsScaleX
    island.y = island.body:getY() * physicsScaleY
    local vx, vy = island.body:getLinearVelocity()
    island.vx = vx * physicsScaleX
    island.vy = vy * physicsScaleY

    if island.x < cx - w / 2 - 400 or island.x > cx + w / 2 + 400 
    or island.y < cy - h / 2 - 400 or island.y > cy + h / 2 + 400 then
      table.insert(remove, idx)
    end

    -- if vx^2 + vy^2 < 1 then
    --   local imp = math.random(0.0, 0.01)
    --   local angle = math.random(0.0, 2.0 * math.pi)
    --   island.body:applyLinearImpulse(imp * math.cos(angle), imp * math.sin(angle))
    -- end
  end

  for i = #remove, 1, -1 do
    if remove[i] then
      local island = table.remove(islands.list, remove[i])
      if island.coin then island.coin.island = nil end
      if island.heart then island.heart.island = nil end
    end
  end

  if #islands.list < 50 then
    local x, y
    local side = math.random(1, 4)
    if side == 1 then
      x = math.random(cx - w/2 - 300, cx + w/2 + 300)
      y = cy - h/2 - 300
    elseif side == 2 then
      x = math.random(cx - w/2 - 300, cx + w/2 + 300)
      y = cy + h/2 + 300
    elseif side == 3 then
      y = math.random(cy - h/2 - 300, cy + h/2 + 300)
      x = cx - w/2 - 300
    elseif side == 4 then
      y = math.random(cy - h/2 - 300, cy + h/2 + 300)
      x = cx + w/2 + 300
    end

    local mind = math.huge
    for _, island in ipairs(islands.list) do
      local d = ((x - island.x) / physicsScaleX)^2 + ((y - island.y) / physicsScaleY)^2
      if d < mind then
        mind = d
      end
    end
    if mind > 16 then
      islands.spawn(x, y)
    end
  end

  table.sort(islands.list, function(a, b) return a.y < b.y end)
end

function islands.draw()
  for _, island  in ipairs(islands.list) do
    local size = sizes[island.type]
    local rx = size * 41
    local ry = size * 30
    love.graphics.draw(islands.assets["island-" .. island.type], island.x, island.y, 0, 1, 1, rx, ry)
    -- love.graphics.ellipse("line", island.x, island.y, rx, ry)
    --
    if island.type == "spawn" then
      local x, y = island.x, island.y
      love.graphics.print("Move with WASD", x - 50, y + 130)
      love.graphics.print("Jump with SPACE", x - 55, y + 142)
      love.graphics.print("Don't FALL", x - 40, y + 154)
    end
  end
end

function islands.drawMasks()
  for _, island in ipairs(islands.list) do
    local size = sizes[island.type]
    local rx = size * 41
    local ry = size * 30
    love.graphics.draw(islands.assets["island-" .. island.type .. "-mask"], island.x, island.y, 0, 1, 1, rx, ry)
  end
end

function islands.find(x, y)
  for idx, island in ipairs(islands.list) do
    local size = sizes[island.type]
    local rx = size * (41 + 4)
    local ry = size * (30 + 4)
    if (x - island.x)^2 / rx^2 + (y - island.y)^2 / ry^2 <= 1 then
      return idx
    end
  end
end

return islands
