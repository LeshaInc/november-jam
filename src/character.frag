uniform Image mask;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords) {
    vec4 texturecolor = Texel(tex, texture_coords);
    if (texturecolor.a < 0.5) {
        texturecolor.a *= Texel(mask, screen_coords / love_ScreenSize.xy).r;
    }
    return texturecolor * color;
}
