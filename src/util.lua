local util = {}

util.loaders = {}

util.loaders["png"] = love.graphics.newImage
util.loaders["ogg"] = function(path) return love.audio.newSource(path, "static") end

function util.loadRecursive(path)
  local results = {}

  local items = love.filesystem.getDirectoryItems(path)
  for _, item in ipairs(items) do
    local path = path .. "/" .. item
    if love.filesystem.getInfo(path).type == "directory" then
      results[item] = util.loadRecursive(path)
    else
      local name, extension = string.match(item, "(.-)%.([^.]+)$")
      if util.loaders[extension] then
        results[name] = util.loaders[extension](path)
      end
    end
  end

  return results
end

function util.ease(t)
  return t < 0.5 and  2 * t * t or -1 + (4 - 2 * t) * t
end

return util
