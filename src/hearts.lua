local util = require("src/util")

local hearts = {list = {}}
local time = 0

function hearts.spawn(x, y, island)
  local heart = {x = x, y = y, island = island, timeOffset = math.random() * 10}
  table.insert(hearts.list, heart)
  return heart
end

function hearts.load()
  hearts.assets = util.loadRecursive("assets/heart")
end

function hearts.reset()
  hearts.list = {}
end

function hearts.update(dt)
  time = time + dt

  local remove = {}
  for idx, heart in ipairs(hearts.list) do
    if heart.island and not heart.remove then
      heart.x = heart.island.x
      heart.y = heart.island.y
    else
      table.insert(remove, idx)
    end
  end

  for i = #remove, 1, -1 do
    if remove[i] then
      table.remove(hearts.list, remove[i])
    end
  end
end

function hearts.draw()
  for _, heart in ipairs(hearts.list) do
    local frame = math.floor((time + heart.timeOffset) * 13.0) % 23 + 1
    image = hearts.assets[string.format("%04d", frame)]
    love.graphics.draw(image, heart.x, heart.y, 0, 1, 1, 25, 37)
  end
end

function hearts.find(cx, cy)
  for _, heart in ipairs(hearts.list) do
    if (heart.x - cx)^2 + (heart.y - cy)^2 < 16^2 then
      return heart
    end
  end
end

return hearts
