local util = require("src/util")

local background = {}

local offset = 0
local grids = {{}, {}, {}}

function background.load()
  background.assets = util.loadRecursive("assets/background")
end

function background.update(dt)
  offset = offset + 100 * dt
end

local function gridify(distance, offs, w, h, px, py, cellSize, grid, img)
  love.graphics.push()
  love.graphics.translate(-(px + offs) / distance + w / 2, -py / distance + h / 2)

  local gridHalfWidth = math.ceil(w / cellSize * 2)
  local gridHalfHeight = math.ceil(h / cellSize * 2)

  local pcx = math.floor((px + offs) / distance / cellSize)
  local pcy = math.floor(py / distance / cellSize)

  for x = pcx - gridHalfWidth, pcx + gridHalfWidth do
    if not grid[x] then grid[x] = {} end
    for y = pcy - gridHalfHeight, pcy + gridHalfHeight do
      if not grid[x][y] then
        local a = math.random() * math.pi * 2
        local sx = math.cos(a) * cellSize * 0.2
        local sy = math.sin(a) * cellSize * 0.2
        grid[x][y] = {sx, sy}
      end

      local ox, oy = (x - 1.5) * cellSize, (y - 1.5) * cellSize
      local sx, sy = unpack(grid[x][y])
      love.graphics.draw(img, math.floor(sx + ox), math.floor(sy + oy))
    end
  end

  for x, _ in pairs(grid) do
    if x < pcx - gridHalfWidth or x > pcx + gridHalfWidth then
      grid[x] = nil
    else
      for y, _ in pairs(grid) do
        if y < pcy - gridHalfHeight or y > pcy + gridHalfHeight then
          grid[x][y] = nil
        end
      end
    end
  end

  love.graphics.pop()
  -- love.graphics.translate(-sx, -sy)
end

function background.draw(px, py, w, h)
  love.graphics.clear(0.090196078431373, 0.56862745098039, 0.76078431372549)

  gridify(10, 0, w, h, px, py, 400, grids[1], background.assets.island)
  gridify(5, offset, w, h, px, py, 100, grids[2], background.assets.cloud)
  gridify(2, offset, w, h, px, py, 220, grids[3], background.assets["cloud-big"])
end

return background
