local util = require("src/util")

local coins = {list = {}}
local time = 0

function coins.spawn(x, y, island)
  local amt = 1
  local rand = math.random(1, 10)
  if rand > 8 then
    amt = 5
  elseif rand > 5 then
    amt = 3
  end

  local coin = {x = x, y = y, island = island, timeOffset = math.random() * 10, amount = amt}
  table.insert(coins.list, coin)
  return coin
end

function coins.load()
  coins.assets = util.loadRecursive("assets/coin")
end

function coins.reset()
  coins.list = {}
end

function coins.update(dt)
  time = time + dt

  local remove = {}
  for idx, coin in ipairs(coins.list) do
    if coin.island and not coin.remove then
      coin.x = coin.island.x
      coin.y = coin.island.y
    else
      table.insert(remove, idx)
    end
  end

  for i = #remove, 1, -1 do
    if remove[i] then
      table.remove(coins.list, remove[i])
    end
  end
end

function coins.draw()
  for _, coin in ipairs(coins.list) do
    local frame = math.floor((time + coin.timeOffset) * 13.0) % 23 + 1
    image = coins.assets[tostring(coin.amount)][string.format("%04d", frame)]
    love.graphics.draw(image, coin.x, coin.y, 0, 1, 1, 25, 37)
  end
end

function coins.find(cx, cy)
  for _, coin in ipairs(coins.list) do
    if (coin.x - cx)^2 + (coin.y - cy)^2 < 16^2 then
      return coin
    end
  end
end

return coins
