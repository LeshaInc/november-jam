local util = require("src/util")
local islands = require("src/islands")
local coins = require("src/coins")
local hearts = require("src/hearts")

local character = {
  assets = {}
}

local directions = {
  [-1] = {[-1] = "nw", [0] = "w", [1] = "sw"},
  [ 0] = {[-1] =  "n", [0] = "n", [1] =  "s"},
  [ 1] = {[-1] = "ne", [0] = "e", [1] = "se"},
}

local gravity = 200

function character.load()
  character.assets = util.loadRecursive("assets/character")
  character.shader = love.graphics.newShader("src/character.frag")

  character.assets.jump:setVolume(0.15)
  character.assets.scream:setVolume(0.2)
  for _, walk in pairs(character.assets.walk) do
    walk:setVolume(0.4)
  end
  character.assets.land:setVolume(0.3)
  character.assets.pickup:setVolume(0.4)
  character.assets.revive:setVolume(0.1)

  character.reset()
end

function character.reset()
  character.x = 0
  character.y = 0
  character.z = 0
  character.jumpVel = {x = 0, y = 0, z = 0}
  character.jumpTime = 0
  character.lastStep = 0
  character.jumpDuration = 0
  character.fell = false
  character.isJumping = false
  character.isRunning = false
  character.runningTime = 0
  character.direction = "s"
  character.score = 0
  character.highScore = tonumber(love.filesystem.read("highscore") or 0)
  character.lives = 1
end

function character.update(dt)
  if character.fell then
    character.z = character.z + character.jumpVel.z * dt
    character.jumpVel.z = character.jumpVel.z - gravity * dt
    return
  end

  local dirx, diry = 0, 0

  if love.keyboard.isDown("w") then
    diry = diry - 1
  end
  if love.keyboard.isDown("s") then
    diry = diry + 1
  end
  if love.keyboard.isDown("a") then
    dirx = dirx - 1
  end
  if love.keyboard.isDown("d") then
    dirx = dirx + 1
  end

  local norm = 1
  if math.abs(dirx) == 1 and math.abs(diry) == 1 then
    norm = 1.41421356237
  end

  character.isRunning = math.abs(dirx) ~= 0 or math.abs(diry) ~= 0
  if character.isRunning then
    if not character.fell then
      character.direction = directions[dirx][diry]
      character.runningTime = character.runningTime + dt
    end
  else
    character.runningTime = 0
    character.lastStep = -999
  end

  dirx = dirx / norm
  diry = diry / norm

  local speed = 130
  if character.isJumping then
    if character.jumpTime > 0.38 then
      speed = 70
    else
      speed = 0
    end
  end

  if not character.fell then
    character.x = character.x + speed * dirx * dt
    character.y = character.y + speed * diry * dt
  end

  character.island = islands.find(character.x, character.y)
  local island = character.island and islands.list[character.island]

  if not character.fell and not character.isJumping and love.keyboard.isDown("space") then
    character.jumpVel.z = 100
    character.jumpDuration = character.jumpVel.z * 2 / gravity
    character.z = 0
    character.isJumping = true
    character.jumpTime = 0
  end

  if character.isJumping and character.jumpTime < 0.4 then
    character.jumpVel.x = 100 * dirx
    character.jumpVel.y = 100 * diry
  end

  if character.isJumping then
    character.jumpTime = character.jumpTime + dt
  end

  if character.isJumping and character.jumpTime > 0.38 then
    if character.jumpTime < 0.4 then
      local pitch = (math.random() - 0.5) * 0.1 + 1
      character.assets.jump:setPitch(pitch)
      love.audio.play(character.assets.jump)
    end

    if not character.initJumpVel then
      character.initJumpVel = {x = island.vx, y = island.vy}
    end

    character.jumpVel.z = character.jumpVel.z - gravity * dt
    character.x = character.x + (character.initJumpVel.x + character.jumpVel.x) * dt
    character.y = character.y + (character.initJumpVel.y + character.jumpVel.y) * dt
    character.z = character.z + character.jumpVel.z * dt
    if character.z < 0 and character.island then
      character.initJumpVel = nil
      if not character.fell then
        love.audio.play(character.assets.land)
      end
      character.z = 0
      character.isJumping = false
    end
  end

  if character.z < 0 or not (character.isJumping and character.jumpTime > 0.38) then
    if island then
      character.x = character.x + island.vx * dt
      character.y = character.y + island.vy * dt
    elseif not character.fell then
      character.lives = character.lives - 1
      if character.lives > 0 then
        local sound = character.assets.revive
        if sound:isPlaying() then sound = sound:clone() end
        sound:play()

        local nearest
        local md = math.huge
        for _, island in ipairs(islands.list) do
          local d = (character.x - island.x)^2 + (character.y - island.y)^2
          if d < md then
            md = d
            nearest = island
          end
        end
        character.x = nearest.x
        character.y = nearest.y
        character.isJumping = false
        character.jumpVel = {0, 0, 0}
      else
        love.audio.play(character.assets.scream)
        character.fell = true
        if not character.isJumping then
          character.jumpVel.z = -200
          character.jumpTime = 9
          character.jumpDuration = 10
        end
      end
    end
  end

  if not character.isJumping and character.isRunning then
    local speed = 0.42
    if character.runningTime - character.lastStep > speed then
      character.lastStep = character.runningTime
      local idx = math.random(1, 4)
      love.audio.play(character.assets.walk[string.format("%04d", idx)])
    end
  end

  local coin = coins.find(character.x, character.y)
  if coin then
    local pitch = (math.random() - 0.5) * 0.5 + 1
    local sound = character.assets.pickup
    if sound:isPlaying() then
      sound = sound:clone()
    end
    sound:setPitch(pitch)
    sound:play()
    character.score = character.score + coin.amount
    coin.remove = true
  end

  local heart = hearts.find(character.x, character.y)
  if heart and character.lives < 3 then
    local sound = character.assets.heartbeat
    if sound:isPlaying() then sound = sound:clone() end
    sound:play()
    character.lives = character.lives + 1
    heart.remove = true
  end
end

function character.draw(maskCanvas)
  love.graphics.setShader(character.shader)
  character.shader:send("mask", maskCanvas)

  if character.fell or character.isJumping then
    local duration = 40.0
    local frame = math.min(math.floor(character.jumpTime * duration), 24) % 25 + 1
    local remaining = character.jumpDuration - character.jumpTime + 0.76
    if character.jumpVel.z < 0 and remaining < duration then
      frame = math.min(math.max(math.floor(remaining * duration), 1), 24)
    end

    if character.fell then love.graphics.setColor(1, 1, 1, 0.5) end
    local image = character.assets["jump-" .. character.direction][string.format("%04d", frame)]
    love.graphics.draw(image, character.x, character.y - character.z, 0, 1, 1, 80, 90)
    if character.fell then love.graphics.setColor(1, 1, 1, 1) end
  else
    local image

    if character.isRunning then
      local frame = math.floor(character.runningTime * 25.0) % 21 + 1
      image = character.assets["run-" .. character.direction][string.format("%04d", frame)]
    else
      image = character.assets["idle-" .. character.direction]
    end

    love.graphics.draw(image, character.x, character.y, 0, 1, 1, 40, 50)
  end

  love.graphics.setShader()
end

return character
